#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

const char* ssid = "IPCS";
const char* password = "1234567890";

ESP8266WebServer server(80);

String page = "";
int idx = 0;
String text = "";
void setup() {

  Serial.begin(115200);
  delay(10);


  Serial.println();
  Serial.println();
  Serial.print("Dang ket noi toi mang ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Da ket noi WiFi");

  Serial.println("Khoi dong Server");

  Serial.println(WiFi.localIP());

  server.on("/data.txt", [](){
    text = String(idx);
    server.send(200, "text/html", text);
  });
  
  server.on("/", [](){
     page = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n";
     page += "<head>";
     page += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
     page += "<script src=\"https://code.jquery.com/jquery-2.1.3.min.js\"></script>";
     page += "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css\">";
     page += "</head>";
     page += "<div class=\"container\">";
     page += "<h1>Dashboard Sensor</h1>";
     page += "<h2 id=\"data\"></h2>";
     page += "<div class=\"row\">";
     page += "</div></div>";
     page += "<script>\r\n";
     page += "var x = setInterval(function() {loadData(\"data.txt\",updateData)}, 1000);\r\n";
     page += "function loadData(url, callback){\r\n";
     page += "var xhttp = new XMLHttpRequest();\r\n";
     page += "xhttp.onreadystatechange = function(){\r\n";
     page += " if(this.readyState == 4 && this.status == 200){\r\n";
     page += " callback.apply(xhttp);\r\n";
     page += " }\r\n";
     page += "};\r\n";
     page += "xhttp.open(\"GET\", url, true);\r\n";
     page += "xhttp.send();\r\n";
     page += "}\r\n";
     page += "function updateData(){\r\n";
     page += " document.getElementById(\"data\").innerHTML = this.responseText;\r\n";
     page += "}\r\n";
     page += "</script>\r\n";

     server.send(200, "text/html", page);
  });

  server.begin();
}

uint64_t t = 0;

void loop() {
  if (millis() - t >= 1000){
    idx++;
    t = millis();
  }
  server.handleClient();
}
